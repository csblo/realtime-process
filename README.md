## Synopsis

Real time process is a simple example of a rest service which can execute any shell on distant machine with the help of socket.io.

## Content

**This project contain :**

**index.html** : an example of client  
**realtime-process.js** : the service  
**process.json** : configuration file referencing authorized processes

## Prerequisites

**NodeJS** must be installed.

## How to run

Open a terminal and go to the root directory of **realtime-process.js**. Run command : **node realtime-process.js**.  
Then open a navigator and enter the address : http://localhost:3333.  
In this example, you have 3 defaults commands : **list**, **print**, **newdirectory**.

## Make works your own processes

### Server side

You just have to configure the **process.json** file. Example :

```
{
    "list":"ls"
}
```

It mean that run the process **list** leads to the execution of **ls** command.

You can add parameters like below :

```
{
    "print":"echo $0"
}
```

It mean that run the process **print x** give **echo x** command.

### Client side

On the client side you just have to call the service at the address : **http://hostname:3333/process.run**.
data is the process to run, for example **print hello**.

```
$.ajax({
    url:'http://localhost:3333/process.run',
    method:'POST',
    data:data,
    contentType:'application/json',
    success:function(res)
    {
        //the result of process
        console.log(res);
    }
});
```

## Motivation

Execute heavily process on server side and return the result to client in real time without any refresh.

## License

MIT License

Copyright 2017 csblo

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.