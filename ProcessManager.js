/**
 * @author cs-blo
 * @date 2016-11-17
 */

/**
 * Construct a manager in order to start processes
 * @param string processFilePath path to json file which contain processes list
 */
function ProcessManager(processFilePath)
{
	this.processFilePath = processFilePath;
};

/**
 * Run a process by name
 * @param string name the name of the process
 * @param array params the process parameters
 * @return boolean true if the process exist in file list
 */
ProcessManager.prototype.run = function(name, params, callback)
{
	//Get command line from process name
	var command = getCommand(name);

	//Command not defined in process.json
	if (!command)
		return false;

	//Set parameters
	for (var i in params)
	{
		const param = params[i];
		command = command.replace("$" + i, param);
	}

	//Execute complete command line
	execute(command, function(a,b) {
		callback(a,b);
	});

	return true;
};

/**
 * @private
 * Get real command line from process name in process.json file
 * @param string name the name of the process
 * @return string real command line linked to the process
 */
function getCommand(name)
{
	const fs = require('fs');
	const path = require('path');
	const StringDecoder = require('string_decoder').StringDecoder;
	const decoder = new StringDecoder('utf8');

	//Read file
	const buffer = fs.readFileSync(path.join(__dirname, 'process.json'));
	const content = decoder.write(buffer);

	//Parse file
	var processList = JSON.parse(content);
	return processList[name];
};

/**
 * @private
 * Execute command line
 * @param string command command line to execute
 * @param callback execution callback
 */
function execute(command, callback)
{
	//setTimeout(function() {
	const exec = require('child_process').exec;
	exec(command, callback);
	//}, 5000);
};

module.exports = ProcessManager;
